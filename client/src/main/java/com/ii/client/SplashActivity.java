package com.ii.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

/**
 * Created by ntkrnlmp.exe on 23.06.2014.
 */
public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        PreferenceManager.setDefaultValues(this, R.xml.notification_preferences, false);
        boolean useSubscribes = pref.getBoolean("use_subscribes_preference", false);

        Intent openMainActivity = new Intent(EchoListActivity.class.getName());
        startActivity(openMainActivity);
        finish();
    }
}
