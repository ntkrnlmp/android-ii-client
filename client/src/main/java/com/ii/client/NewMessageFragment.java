package com.ii.client;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ii.client.ii.AsyncTasks.SendMessageAsyncTask;
import com.ii.client.ii.AsyncTasks.UpdateMessageAsyncTask;
import com.ii.client.ii.Client;
import com.ii.client.ii.ClientSingleton;
import com.ii.client.ii.Models.OutgoingMessage;

/**
 * Created by ntkrnlmp.exe on 21.06.2014.
 */
public class NewMessageFragment extends Fragment {

    EditText subjectTextBox;
    EditText messageTextBox;
    EditText toTextBox;
    String id;
    String to;
    int localId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_new_message, container, false);
        setHasOptionsMenu(true);

        Bundle args = getArguments();

        subjectTextBox = (EditText) rootView.findViewById(R.id.subjectTextBox);
        messageTextBox = (EditText) rootView.findViewById(R.id.messageTextBox);
        toTextBox = (EditText) rootView.findViewById(R.id.toTextBox);

        if(args != null) {
            localId = args.getInt("LocalId");

            if(localId != 0) {
                Toast.makeText(getActivity(), "Режим редактирования сообщения.", Toast.LENGTH_SHORT).show();
                String text = args.getString("Message");
                if(text != null) {
                    messageTextBox.setText(text);
                }

                String subj = args.getString("Subject");

                if(subj != null) {
                    if (!subj.contains("Re:")) {
                        subj = "Re: " + subj;
                    }
                }

                subjectTextBox.setText(subj);
            }
            else {
                String subj = args.getString("Subject");

                if (!subj.contains("Re:")) {
                    subj = "Re: " + subj;
                }

                subjectTextBox.setText(subj);
                id = args.getString("Id");
                to = args.getString("To");

                if(to != null) {
                    toTextBox.setText(to);
                } else {
                    toTextBox.setText("All");
                }

                String quote = args.getString("Quote");
                if(quote != null) {
                    messageTextBox.setText(">" + quote.trim().replaceAll("\n", "\n>"));
                }
            }
        }
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_message_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send_message:

                if(messageTextBox.getText().toString().trim().length() == 0) {
                    Toast toast = Toast.makeText(getActivity(),
                            "Нельзя отправлять пустое сообщение",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 20);
                    toast.show();
                    return true;
                }

                if(subjectTextBox.getText().toString().trim().length() == 0) {
                    Toast toast = Toast.makeText(getActivity(),
                            "Нельзя отправлять сообщение без темы",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.BOTTOM, 0, 20);
                    toast.show();
                    return true;
                }

                if(toTextBox.getText().toString().trim().length() == 0) {
                    toTextBox.setText("All");
                }

                Client client = ClientSingleton.getClient();
                client.isOfflineMode = EchoDetailFragment.isNetworkAvailable(getActivity());

                // обновление неотправленного сообщения
                if(localId != 0 ) {
                    OutgoingMessage updatedMessage = new OutgoingMessage();

                    updatedMessage.setSubject(subjectTextBox.getText().toString());
                    updatedMessage.setMessage(messageTextBox.getText().toString());
                    updatedMessage.localId = localId;

                    new UpdateMessageAsyncTask(client, (Fragment)this).execute(updatedMessage);
                    getFragmentManager().popBackStackImmediate("echodetail", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                }

                // отправка нового сообщения
                OutgoingMessage message = new OutgoingMessage();
                message.setEchoArea(client.getCurrentEchoArea());

                if(id == null) {
                    message.setReplyTo(null);
                    message.setMessageTo(toTextBox.getText().toString());
                }
                else {
                    message.setReplyTo(id);
                    message.setMessageTo(toTextBox.getText().toString());
                }

                message.setSubject(subjectTextBox.getText().toString());
                message.setMessage(messageTextBox.getText().toString());

                new SendMessageAsyncTask(client, (Fragment)this).execute(message);
                getFragmentManager().popBackStackImmediate("echodetail", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
