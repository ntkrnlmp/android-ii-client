package com.ii.client;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.ii.client.dummy.DummyContent;
import com.ii.client.ii.Client;
import com.ii.client.ii.ClientSingleton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

/**
 * Created by ntkrnlmp.exe on 22.06.2014.
 */
public class ClientService extends Service {
    private IBinder mBinder = new ServerBinder();
    private boolean mRunning = false;
    private Timer mTimer;
    List<String> echos;

    private int state;
    private HashMap<String, Integer> prev_state;
    private static int interval = 1000;
    private PoolingTimerTask task;
    private HashMap<String, Integer> notifyId;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Service", "onCreate");

        prev_state = new HashMap<String, Integer>();
        notifyId = new HashMap<String, Integer>();
        echos = new ArrayList<String>();
        for(int i=0; i < DummyContent.ITEMS.size(); ++i) {
            echos.add(DummyContent.ITEMS.get(i).content);
            notifyId.put(DummyContent.ITEMS.get(i).content, i);
        }

        mTimer = new Timer();

    }

    public static void setInterval(int value) {
        interval = value;
    }

    private void notificate(String echoName, int messages_count) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(echoName)
                        .setContentText("Новых сообщений: " + messages_count);
        //TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        //Intent resultIntent = new Intent(getApplicationContext(), EchoListActivity.class);
        //stackBuilder.addParentStack(EchoListActivity.class);
        //stackBuilder.addNextIntent(resultIntent);
        //PendingIntent resultPendingIntent =
        //        stackBuilder.getPendingIntent(
        //                0,
        //                PendingIntent.FLAG_UPDATE_CURRENT
        //        );
        //mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notifyId.get(echoName), mBuilder.build());
    }

    @Override
    public IBinder onBind(Intent arg0) {
        Log.d("Service", "onBind()");
        mRunning = true;
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("Service", "onUnbind()");
        mRunning = false;
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Service", "onDestroy()");
        mTimer.cancel();
        mRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "onStartCommand()");
        if(!mRunning) {
            mRunning = true;

            mTimer = new Timer();
            task = new PoolingTimerTask();
            Log.d("Service", "Starting new task");

            interval = intent.getIntExtra("interval", 36000000);
            mTimer.schedule(task, interval, interval);
            mRunning = true;
        } else {
            if(intent.getBooleanExtra("intervalChanged", false)) {
                interval = intent.getIntExtra("interval", 36000000);
                mTimer.cancel();
                mTimer.purge();
                mTimer = new Timer();
                mTimer.schedule(new PoolingTimerTask(), interval, interval);
                mRunning = true;
                Log.d("Service", "Change interval and restart");
            }
        }

        return super.onStartCommand(intent, flags, startId);

    }

    public class ServerBinder extends Binder {

        public ClientService getService() {
            return ClientService.this;
        }

    }

    public class PoolingTimerTask extends TimerTask {
        @Override
        public void run() {
            if(mRunning) {
                try {
                    Log.d("Service", "fetching new messages");
                    if(echos.size() < DummyContent.ITEMS.size()) {
                        for(int i=0; i < DummyContent.ITEMS.size(); ++i) {
                            echos.add(DummyContent.ITEMS.get(i).content);
                        }
                    }
                    // FIXME: заменить на observer паттерн
                    for(String echo : echos) {
                        state = ClientSingleton.getClient().getNewMessagesCount(echo);
                        if(!prev_state.containsKey(echo)) {
                            prev_state.put(echo, 0);
                            continue;
                        }
                        if (state > 0 & state > prev_state.get(echo)) {
                            notificate(echo, state);
                            prev_state.put(echo, state);
                        } else if (prev_state.get(echo) > state){
                            prev_state.put(echo, 0);
                            state = 0;
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                mTimer.cancel();
            }
        }
    }
}
