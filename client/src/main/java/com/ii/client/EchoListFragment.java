package com.ii.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.ii.client.dummy.DummyContent;
import com.ii.client.ii.AsyncTasks.RetrieveEchoListTask;
import com.ii.client.ii.Client;
import com.ii.client.ii.ClientSingleton;
import com.ii.client.ii.Database.PlainTextDatabase;
import com.ii.client.ii.Database.SQLiteDataBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A list fragment representing a list of Messages. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link EchoDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class EchoListFragment extends ListFragment {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(String id) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EchoListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("Debug", "EchoListFragment: onCreate");

        initClient();

        //DEBUG:
        //getActivity().deleteDatabase("ii_db");
    }

    private void initClient() {
        Log.d("Debug", "initClient");
        Client client = ClientSingleton.getClient();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

        client.setNode(pref.getString("server_preference", null));
        client.setAuth(pref.getString("auth_preference", null));

        try {
            client.setMaxBundleCount(Integer.parseInt(pref.getString("bundle_count_preference", "60")));
        }
        catch (Exception e)
        {
            client.setMaxBundleCount(60);
        }

        int dbType = 1;
        try {
            dbType = Integer.parseInt(pref.getString("database_type_preference", "1"));
        }
        catch (Exception e)
        {
            //fallback to standart
            dbType = 1;
        }

        switch(dbType)
        {
            case 1:
                client.setDatabase(new PlainTextDatabase());
                break;
            case 2:
                client.setDatabase(new SQLiteDataBase(this.getActivity()));
                break;
            default:
                client.setDatabase(new PlainTextDatabase());
                //new PlainTextDatabase());
                break;
        }

        if(client.getNode() == null || client.getNode().trim() == "") {
            Intent intent = new Intent(this.getActivity(), PrefActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(intent, 18);
            Toast.makeText(this.getActivity().getApplication(), "Не указан адрес ноды.", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean useSubscribes = pref.getBoolean("use_subscribes_preference", false);

        if(useSubscribes) {
            Log.d("Debug", "useSubscribes");
            String buf = pref.getString("subscribes", null);
            if(buf != null) {
                List<String> list = new ArrayList<String>(Arrays.asList(buf.split(";")));
                setEchoList((ArrayList<String>) list);
            }
            else {
                setEchoList(null);
            }
        } else {
            Log.d("Debug", "don't use subscribes");
            RetrieveEchoListTask task = new RetrieveEchoListTask(client, this);
            task.execute();
        }

        boolean useNotifications = pref.getBoolean("notification_enabled_preference", false);
        if(useNotifications) {
            Intent intent = new Intent(new Intent(getActivity(), ClientService.class));
            int val = Integer.parseInt(pref.getString("notification_pooling_interval_preference", "3600"));
            intent.putExtra("interval", val * 60000);
            getActivity().stopService(intent);
            getActivity().startService(intent);
        }
        else {
            getActivity().stopService(new Intent(getActivity(), ClientService.class));
        }
    }


    public void setEchoList(ArrayList<String> strings) {
        DummyContent.ITEM_MAP.clear();
        DummyContent.ITEMS.clear();

        if(strings != null) {

            int i = 1;
            for (String s : strings) {
                DummyContent.addItem(new DummyContent.DummyItem(String.valueOf(i++), s));
            }
            setListAdapter(new ArrayAdapter<DummyContent.DummyItem>(
                    getActivity(),
                    android.R.layout.simple_list_item_activated_1,
                    android.R.id.text1,
                    DummyContent.ITEMS));
        }
        else {
            setListAdapter(null);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("Debug", "EchoListFragment: onCreateView");
        return inflater.inflate(R.layout.echo_row_layout, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("Debug", "EchoListFragment: onResume");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        setHasOptionsMenu(true);

        try {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String vers = pInfo.versionCode + "." + pInfo.versionName;

            if (!pref.getString("client_ver", "").equals(vers)) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("client_ver", vers);
                editor.commit();

                final AlertDialog.Builder dialog = new AlertDialog.Builder(this.getActivity());
                dialog.setTitle("Ваш ii клиент обновлен!");
                dialog.setMessage(R.string.update_message);

                dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dialog.create().show();
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(DummyContent.ITEMS.get(position).id);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:

                new AsyncTask<Activity, Void, Void>() {
                    @Override
                    protected Void doInBackground(Activity... contexts) {
                        Intent intent = new Intent(contexts[0], PrefActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivityForResult(intent, 18);
                        return null;
                    }
                }.execute(this.getActivity());

                //Intent intent = new Intent(this.getActivity(), PrefActivity.class);
                //startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Debug", "onActivityResult, requestCode = " + requestCode);
        if(requestCode == 18) {
            initClient();
        }
    }
}
