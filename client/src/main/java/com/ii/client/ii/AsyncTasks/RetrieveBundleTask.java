package com.ii.client.ii.AsyncTasks;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.ListView;

import com.ii.client.EchoDetailActivity;
import com.ii.client.EchoDetailFragment;
import com.ii.client.MessageFactory;
import com.ii.client.ii.Client;
import com.ii.client.ii.IDownloadProgressUpdate;
import com.ii.client.ii.Models.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */
public class RetrieveBundleTask extends AsyncTask<Object, String, List<Message>> implements IDownloadProgressUpdate {

    ProgressDialog dialog;
    Client client;
    EchoDetailFragment fragment;
    Exception e;
    public boolean isLoadingData = false;

    public RetrieveBundleTask(Client client, EchoDetailFragment fragment) {
        this.client = client;
        this.fragment = fragment;
        dialog = new ProgressDialog(fragment.getActivity());
        dialog.setTitle("Загружаю сообщения");
        dialog.setMessage("Пожалуйста, подождите...");
        dialog.setCancelable(false);
    }


    @Override
    protected void onPreExecute() {
        dialog.show();
        isLoadingData = true;
    }

    @Override
    protected void onPostExecute(List<Message> messages) {
        dialog.dismiss();

        if (this.e != null) {
            if (this.e != null) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(fragment.getActivity());
                dialog.setTitle("Ошибка");

                dialog.setMessage(getExceptionMessage());

                dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                dialog.create().show();
                return;
            }
        }

        fragment.bundleDownloadFinished(messages);
        isLoadingData = false;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        dialog.setMessage(values[0]);
    }

    @Override
    protected List<Message> doInBackground(Object... objects) {
        List<Message> messages = null;
        try {
            messages = (List<Message>) client.getDatabase().GetMessages((String) objects[0], (Integer) objects[1], (Integer) objects[2], this);
        } catch (Exception e) {
            e.printStackTrace();
            this.e = e;
        }
        return messages;
    }

    private String getExceptionMessage() {
        return e.getMessage();
    }

    @Override
    public void raiseProgressUpdate(String... values) {
        publishProgress(values);
    }
}
