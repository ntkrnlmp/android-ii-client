package com.ii.client.ii.Database;

import com.ii.client.ClientService;
import com.ii.client.ii.ClientSingleton;
import com.ii.client.ii.IDownloadProgressUpdate;
import com.ii.client.ii.Models.Message;
import com.ii.client.ii.Models.OutgoingMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */
public class PlainTextDatabase implements IDatabase {

    HashMap<String, List<String>> echos;
    HashMap<String, ArrayList<Message>> messages;

    public PlainTextDatabase() {
        echos = new HashMap<String, List<String>>();
        messages = new HashMap<String, ArrayList<Message>>();
    }

    @Override
    public void CreateEcho(String echoName) {

    }

    @Override
    public void CreateEchos(Iterable<String> echos) {

    }

    @Override
    public void AddMessageToEcho(String echoName, Message message) {

    }

    @Override
    public void AddMessage(Message message) {

    }

    @Override
    public boolean CanOffline() {
        return false;
    }

    @Override
    public void AddMessagesToEcho(String echoName, Iterable<Message> messages, int index) {
        List<Message> exists = this.messages.get(echoName);
        if(exists == null) {
            this.messages.put(echoName, (ArrayList<Message>) messages);
        } else {

            // хот фикс :D
            // надо посмотреть в getbundle, прилетает невалидный индекс
            if(index >= exists.size()) {
                index = 0;

                if(index < 0) {
                    index = exists.size();
                }
            }
            exists.addAll(index, (java.util.Collection<? extends Message>) messages);
        }
    }

    @Override
    public Iterable<String> GetEchoMessageList(String echoName) {
        if(echos.containsKey(echoName)) {
            return echos.get(echoName);
        }
        return null;
    }

    @Override
    public void AddSendLaterMessage(OutgoingMessage message) {

    }

    @Override
    public void UpdateSendLaterMessage(OutgoingMessage message) {

    }

    @Override
    public void SetEchoMessageList(String echoName, Iterable<String> messageList) {
        echos.put(echoName, (List<String>) messageList);
    }

    @Override
    public Iterable<Message> GetMessages(Iterable<String> messagesHashId) {
        return null;
    }

    @Override
    public Iterable<Message> GetMessages(String echoName) {
        if(messages.containsKey(echoName)) {
            return messages.get(echoName);
        }
        return null;
    }

    @Override
    public Iterable<Message> GetMessages(String echoName, int offset, int count, IDownloadProgressUpdate updater) throws Exception {

        if(!messages.containsKey(echoName)) {
            messages.put(echoName, new ArrayList<Message>());
        }

        Iterable<Message> downloaded = ClientSingleton.getClient().getBundleReverse(echoName, offset, count);

        // а вот тут надо подумать
        //if(downloaded == null)
        //    return null;

        if(messages.get(echoName).size() < offset + count) {
            count = messages.get(echoName).size() - offset;
        }
        return messages.get(echoName).subList(offset, count);
    }

    @Override
    public Message GetMessage(String messageHashId) {
        return null;
    }

    @Override
    public Iterable<String> GetEchos() {
        return echos.keySet();
    }
}
