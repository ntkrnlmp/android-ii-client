package com.ii.client.ii.AsyncTasks;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;

import com.ii.client.EchoListFragment;
import com.ii.client.dummy.DummyContent;
import com.ii.client.ii.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */
public class RetrieveEchoListTask extends AsyncTask<Void, Void, Iterable<String>> {
    EchoListFragment fragment;
    Client client;
    Exception e;

    public RetrieveEchoListTask(Client client, EchoListFragment fragment) {
        this.client = client;
        this.fragment = fragment;
    }

    @Override
    protected void onPostExecute(Iterable<String> strings) {

        if(this.e != null) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(fragment.getActivity());
            dialog.setTitle("Ошибка");
            dialog.setMessage(e.getMessage());

            dialog.setNeutralButton("OK",  new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            dialog.create().show();
            return;
        }

        fragment.setEchoList((ArrayList<String>) strings);
    }

    @Override
    protected Iterable<String> doInBackground(Void... voids) {

        Iterable<String> echos = null;
        try {
            echos = client.getEchos();
        } catch (IOException e) {
            this.e = e;
            e.printStackTrace();
        }

        return echos;
    }
}
