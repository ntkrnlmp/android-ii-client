package com.ii.client.ii.AsyncTasks;

import android.os.AsyncTask;
import android.util.Base64;

import com.ii.client.MessageFactory;
import com.ii.client.ii.Models.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ntkrnlmp.exe on 17.06.2014.
 */
public class RetrieveSortedByThemeBundle extends AsyncTask<Object, Void, HashMap<String, Message>> {
    @Override
    protected HashMap<String, Message> doInBackground(Object... objects) {
        List<String> messagesId = (List<String>) objects[0];
        int maxBundleCount =  (Integer) objects[1];
        String url = (String) objects[2];

        String line;

        URLConnection webClient = null;

        StringBuilder bundleUrl = new StringBuilder();

        int downloadedCount = 0;

        HashMap<String, Message> messages = new HashMap<String, Message>();


        // фикс ошибки url is too large - запрашиваем по MaxBundleCount сообщений в рамках одного бандла
        while(downloadedCount < messagesId.size())
        {
            // либо выкачиваем MaxBundleCount, либо остаток, который меньше чем MaxBundleCount
            int upperValue = downloadedCount + maxBundleCount < messagesId.size() ? maxBundleCount : messagesId.size() - downloadedCount;
            for (int i = downloadedCount; i < downloadedCount + upperValue; ++i)
            {
                bundleUrl.append("/" + messagesId.get(i));
            }

            //FIXME вынести отсюда нахер это все
            try {
                webClient = (HttpURLConnection) new URL(url + bundleUrl.toString()).openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = null;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(webClient.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            StringBuilder sb = new StringBuilder();

            try {
                while((line = bufferedReader.readLine()) != null) {
                    sb.append(line + "\n");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String raw = sb.toString();
            //FIXME
            raw = raw.substring(raw.indexOf(':') + 1, raw.length());

            String patternStr = "([a-zA-Z+-/0-9=]+:[a-zA-Z+-/0-9=]+)";
            Pattern pattern = Pattern.compile(patternStr);
            Matcher match = pattern.matcher(raw);

            String plainText = null;

            while(match.find()) {
                byte[] bytes = Base64.decode(match.group().substring(match.group().indexOf(':'), match.group().length()), Base64.DEFAULT);
                try {
                    plainText = new String(bytes, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Message m = MessageFactory.ParseMessage(plainText);
                m.setMsgId("msgid");
                messages.put(m.getSubject(),m);
            }

            bundleUrl = new StringBuilder();

            downloadedCount += maxBundleCount;
        }

        return messages;
    }
}
