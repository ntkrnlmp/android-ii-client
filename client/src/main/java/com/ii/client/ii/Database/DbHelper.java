package com.ii.client.ii.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ntkrnlmp.exe on 04.07.2014.
 */
public class DbHelper extends SQLiteOpenHelper {

    static String dbName = "ii_db";
    static int version = 1;
    public DbHelper(Context context)
    {
        super(context, dbName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // список эх
        sqLiteDatabase.execSQL("create table echos ("
                + "id integer primary key autoincrement,"
                + "name text, "
                + "description text"
                + ");"
        );

        // список сообщений
        sqLiteDatabase.execSQL("create table messages ("
                        + "id integer primary key autoincrement, "
                        + "msgId text, "
                        + "tags text, "
                        + "echoarea text, "
                        + "date DATETIME CURRENT_TIMESTAMP, "
                        + "messageFrom text, "
                        + "address text, "
                        + "messageTo text, "
                        + "subject text, "
                        + "message text);"
        );

        // связь эхи-сообщения
        sqLiteDatabase.execSQL("create table echomessages ("
                        + "id integer primary key autoincrement, "
                        + "echo_id integer NOT NULL, "
                        + "message_id integer NOT NULL, "
                        + "FOREIGN KEY(message_id) REFERENCES messages (id), "
                        + "FOREIGN KEY(echo_id) REFERENCES echos (id));"
        );

        // сообщения для отложенной отправки
        sqLiteDatabase.execSQL("create table messages_to_send ("
                        + "id integer primary key autoincrement, "
                        + "echoarea text, "
                        + "messageto text, "
                        + "subject text, "
                        + "replyto text, "
                        + "message text);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }
}
