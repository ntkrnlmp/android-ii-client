package com.ii.client.ii;

import android.content.SharedPreferences;

import com.ii.client.ii.Database.PlainTextDatabase;

/**
 * Created by ntkrnlmp.exe on 21.06.2014.
 */
public class ClientSingleton {
    private static Client client;
    public static Client getClient() {
        if(client == null) {
            client = new Client(null);
        }

        return client;
    }

}
