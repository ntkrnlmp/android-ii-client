package com.ii.client.ii.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ii.client.ii.ClientSingleton;
import com.ii.client.ii.IDownloadProgressUpdate;
import com.ii.client.ii.Models.Message;
import com.ii.client.ii.Models.OutgoingMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by ntkrnlmp.exe on 04.07.2014.
 */
public class SQLiteDataBase implements IDatabase {

    private DbHelper dbHelper;

    public SQLiteDataBase(Context context){
        dbHelper = new DbHelper(context);
    }

    @Override
    public void CreateEcho(String echoName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", echoName);
        cv.put("description", "");
        db.insert("echos", null, cv);
        db.close();
    }

    @Override
    public void CreateEchos(Iterable<String> echos) {

    }

    @Override
    public void AddMessageToEcho(String echoName, Message message) {

    }

    @Override
    public void AddMessage(Message m) {
        ContentValues cv;
        long rowid;
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor c = db.query("echos", null, "name = ?", new String[] {m.getEchoArea()}, null, null, null);
        if(!c.moveToFirst()) {
            Log.d("SQLiteDatabase", "(AddMessage) no echo found.");
            c.close();
            db.close();
            return;
        }

        c.close();

        int echo_id = c.getInt(c.getColumnIndex("id"));

        cv = new ContentValues();
        cv.put("msgId", m.getMsgId());
        cv.put("tags", m.getTags());
        cv.put("echoarea", m.getEchoArea());
        cv.put("date", getDateTime(m.getDate()));
        cv.put("messageFrom", m.getMessageFrom());
        cv.put("address", m.getAddress());
        cv.put("messageTo", m.getMessageTo());
        cv.put("subject", m.getSubject());
        cv.put("message", m.getText());
        rowid = db.insert("messages", null, cv);

        // запись в таблицу echomessages
        cv = new ContentValues();
        cv.put("echo_id", echo_id);
        cv.put("message_id", rowid);
        db.insert("echomessages", null, cv);
        db.close();
    }

    private String getDateTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date);
    }

    @Override
    public void AddSendLaterMessage(OutgoingMessage m) {
        ContentValues cv;
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        cv = new ContentValues();
        cv.put("echoarea", m.getEchoArea());
        cv.put("messageto", m.getMessageTo());
        cv.put("subject", m.getSubject());
        cv.put("replyto", m.getReplyTo());
        cv.put("message", m.getMessage());
        db.insert("messages_to_send", null, cv);

        db.close();
    }

    @Override
    public void AddMessagesToEcho(String echoName, Iterable<Message> messages, int index) {
        ContentValues cv;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        HashMap<String, Integer> echos = new HashMap<String, Integer>();
        long rowid;

        for(Message m : messages)
        {
            if(!echos.containsKey(m.getEchoArea())) {
                Cursor c = db.query("echos", null, "name = ?", new String[] {m.getEchoArea()}, null, null, null);
                if(c.moveToFirst())
                {
                    echos.put(m.getEchoArea(), c.getInt(c.getColumnIndex("id")));
                }
                else
                {
                    // заменить вызовом метода CreateEcho()
                    cv = new ContentValues();
                    cv.put("name", m.getEchoArea());
                    cv.put("description", "");
                    db.insert("echos", null, cv);
                    c.close();
                    c = db.query("echos", null, "name = ?", new String[] {m.getEchoArea()}, null, null, null);
                    c.moveToFirst();
                    echos.put(m.getEchoArea(), c.getInt(c.getColumnIndex("id")));
                }
                c.close();
            }

            // запись в таблицу messages
            cv = new ContentValues();
            cv.put("msgId", m.getMsgId());
            cv.put("tags", m.getTags());
            cv.put("echoarea", m.getEchoArea());
            cv.put("date", getDateTime(m.getDate()));
            cv.put("messageFrom", m.getMessageFrom());
            cv.put("address", m.getAddress());
            cv.put("messageTo", m.getMessageTo());
            cv.put("subject", m.getSubject());
            cv.put("message", m.getText());
            rowid = db.insert("messages", null, cv);

            // запись в таблицу echomessages
            cv = new ContentValues();
            cv.put("echo_id", echos.get(m.getEchoArea()));
            cv.put("message_id", rowid);
            db.insert("echomessages", null, cv);
        }

        db.close();
    }

    private void sendMessages() throws Exception {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = "SELECT echoarea, messageto, subject, replyto, message FROM messages_to_send";

        Cursor c = db.rawQuery(query, null);
        if(c.moveToFirst())  {
            do {
                // парсинг сообщения
                OutgoingMessage m = new OutgoingMessage();
                m.subject = c.getString(c.getColumnIndex("subject"));
                m.echoArea = c.getString(c.getColumnIndex("echoarea"));;
                m.messageTo = c.getString(c.getColumnIndex("messageto"));;
                m.replyTo = c.getString(c.getColumnIndex("replyto"));;
                m.message = c.getString(c.getColumnIndex("message"));

                ClientSingleton.getClient().sendMessage(m);
                // отправка сообщения
            } while (c.moveToNext());
        }
        db.execSQL("DELETE FROM messages_to_send");
        c.close();
        db.close();
    }

    @Override
    public Iterable<Message> GetMessages(String echoName, int offset, int count, IDownloadProgressUpdate updater) throws Exception {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // проверка существования эхи
        Cursor check = db.query("echos", null, "name = ?", new String[] {echoName}, null, null, null);

        List<Message> messages = null;
        boolean firstLoad = false;

        if(!check.moveToFirst()) {
            CreateEcho(echoName);
        }

        check.close();
        db.close();

        db = dbHelper.getReadableDatabase();

        if(updater != null) {
            updater.raiseProgressUpdate("Получение списка загруженных сообщений...");
        }
        String query = "SELECT m.msgId "
                + "FROM echomessages em "
                + "INNER JOIN echos e ON em.echo_id = e.id AND e.name = ? "
                + "INNER JOIN messages m ON m.id = em.message_id";


        Cursor curs = db.rawQuery(query, new String[] {echoName});
        if(curs.getCount() == 0)
        {
            // первая загрузка сообщений.
            if (!ClientSingleton.getClient().isOfflineMode) {
                sendMessages();
                messages = (List<Message>) ClientSingleton.getClient().getBundleDirect(echoName, 0, Integer.MAX_VALUE, updater);
                Collections.reverse(messages);
            } else {
                throw new Exception("Вы не подключены к сети.\nВ локальной базе данных отсутствуют сообщения из данной эхоконференции.");
            }
        }
        else
        {
            // подгрузка новых сообщений, если требуется\
            // проверка нужна, чтобы во время скролла не подгружались новые сообщения с ноды
            if(offset == 0) {
                if(!ClientSingleton.getClient().isOfflineMode) {
                    sendMessages();
                    messages = (List<Message>) ClientSingleton.getClient().getBundleDirect(echoName, offset, Integer.MAX_VALUE, updater);
                    if (messages != null) {
                        Collections.reverse(messages);
                        count = count - messages.size();
                        offset += messages.size();
                    }
                }

                db = dbHelper.getReadableDatabase();
                // получение и вывод неотправленных сообщений из данной эхи
                query = "SELECT mts.id, mts.replyto, mts.echoarea, mts.messageto, mts.subject, mts.message FROM messages_to_send mts WHERE mts.echoarea = \'" + echoName + "\'";
                Cursor uc = db.rawQuery(query, null);

                if(uc.moveToFirst()) {
                    if(messages == null) {
                        messages = new ArrayList<Message>();
                    }

                    do {
                        messages.add(0, parseUnsendedMessage(uc));
                    } while (uc.moveToNext());
                }

                uc.close();
                db.close();
            }

            Cursor c;

            if(updater != null) {
                updater.raiseProgressUpdate("Чтение сообщений");
            }
            // получить список сообщений
            query = "SELECT m.msgId, m.tags, m.echoarea, m.date, m.messageFrom, m.address, "
                    + "m.messageTo, m.subject, m.message "
                    + "FROM echomessages em INNER JOIN echos e ON em.echo_id = e.id AND e.name = \'" + echoName + "\' "
                    + "INNER JOIN messages m ON m.id = em.message_id "
                    + "ORDER BY m.id DESC "
                    + "LIMIT " + count + " OFFSET " + offset;
            db = dbHelper.getReadableDatabase();

            c = db.rawQuery(query, null);

            if(c.moveToFirst())
            {
                updater.raiseProgressUpdate("Чтение сообщений (0 из " + c.getCount() + ")");

                if(messages == null) {
                    messages = new ArrayList<Message>();
                }

                do {
                    if(updater != null) {
                        updater.raiseProgressUpdate("Чтение сообщений (" + c.getPosition() + " из " + c.getCount() + ")");
                    }
                    messages.add(parseMessage(c));
                } while (c.moveToNext());

                c.close();
                db.close();
            }
        }


        return messages;
    }

    @Override
    public boolean CanOffline() {
        return true;
    }


    private Message parseUnsendedMessage(Cursor c) {
        Message m = new Message();
        m.setEchoArea(c.getString(c.getColumnIndex("echoarea")));
        m.setDate(Calendar.getInstance().getTime());
        m.setMessageFrom("Вы");
        m.setMessageTo(c.getString(c.getColumnIndex("messageto")));
        m.setSubject(c.getString(c.getColumnIndex("subject")));
        m.setText(c.getString(c.getColumnIndex("message")));
        m.setIsLocal(true);
        m.setLocalId(c.getInt(c.getColumnIndex("id")));

        return m;
    }

    @Override
    public Iterable<String> GetEchoMessageList(String echoName) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String query = "SELECT m.msgId "
                + "FROM echomessages em "
                + "INNER JOIN echos e ON em.echo_id = e.id AND e.name = ? "
                + "INNER JOIN messages m ON m.id = em.message_id";

        Cursor c = db.rawQuery(query, new String[] {echoName});
        if(c.getCount() == 0)
        {
            c.close();
            db.close();
            return null;
        }

        List<String> messages = new ArrayList<String>();

        if(c.moveToFirst())
        {
            do {
                messages.add(c.getString(c.getColumnIndex("msgId")));
            } while (c.moveToNext());
        }

        c.close();
        db.close();

        return messages;
    }

    @Override
    public void SetEchoMessageList(String echoName, Iterable<String> messageList) {
        // ничего
    }

    @Override
    public void UpdateSendLaterMessage(OutgoingMessage message) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = "UPDATE messages_to_send SET subject = \'" + message.subject + "\', "
                     + "message = \'" + message.message + "\' WHERE id = " + message.localId + "";
        db.execSQL(query);

        db.close();
    }

    @Override
    public Iterable<Message> GetMessages(Iterable<String> messagesHashId) {
        return null;
    }

    private Message parseMessage(Cursor c)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Message m = new Message();
        m.setMsgId(c.getString(c.getColumnIndex("msgId")));
        m.setTags(c.getString(c.getColumnIndex("tags")));
        m.setEchoArea(c.getString(c.getColumnIndex("echoarea")));
        try {
            m.setDate(format.parse(c.getString(c.getColumnIndex("date"))));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        m.setMessageFrom(c.getString(c.getColumnIndex("messageFrom")));
        m.setAddress(c.getString(c.getColumnIndex("address")));
        m.setMessageTo(c.getString(c.getColumnIndex("messageTo")));
        m.setSubject(c.getString(c.getColumnIndex("subject")));
        m.setText(c.getString(c.getColumnIndex("message")));

        return m;
    }


    @Override
    public Iterable<Message> GetMessages(String echoName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = "SELECT m.msgId, m.tags, e.Name as echoarea, m.date, m.messageFrom, m.address, "
                + "m.messageTo, m.subject, m.message "
                + "FROM messages AS m "
               + "INNER JOIN echos AS e ON m.echoarea = e.id "
                + "WHERE echoarea = ?";
        Cursor c = db.rawQuery(query, new String[] {echoName});
        if(c.getCount() == 0)
        {
            c.close();
            db.close();
            return null;
        }

        List<Message> messages = new ArrayList<Message>();

        if(c.moveToFirst())
        {
            do {
                messages.add(parseMessage(c));
            } while (c.moveToNext());
        }

        c.close();
        db.close();

        return messages;
    }

    @Override
    public Message GetMessage(String messageHashId) {
        return null;
    }

    @Override
    public Iterable<String> GetEchos() {
        // TODO
        return null;
    }
}
