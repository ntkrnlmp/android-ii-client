package com.ii.client.ii.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.widget.Toast;

import com.ii.client.ii.Client;
import com.ii.client.ii.Models.OutgoingMessage;

/**
 * Created by ntkrnlmp.exe on 05.08.2014.
 */

/**
 * Created by ntkrnlmp.exe on 21.06.2014.
 */
public class UpdateMessageAsyncTask extends AsyncTask<OutgoingMessage, Void, Void> {

    Client client;
    Exception e;
    Context fragment;

    public UpdateMessageAsyncTask(Client client, Fragment fragment) {
        this.client = client;
        this.fragment = fragment.getActivity().getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Toast toast = Toast.makeText(fragment,
                "Обновление сообщения...",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 50);
        toast.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if(this.e != null) {
            Toast toast = Toast.makeText(fragment,
                    e.getMessage(),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        } else {

            Toast toast = Toast.makeText(fragment,
                    "Сообщение обновлено.",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        }
    }

    @Override
    protected Void doInBackground(OutgoingMessage... outgoingMessages) {
        try {
            if(client.isOfflineMode) {
                client.getDatabase().UpdateSendLaterMessage(outgoingMessages[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.e = e;
        }
        return null;
    }
}
