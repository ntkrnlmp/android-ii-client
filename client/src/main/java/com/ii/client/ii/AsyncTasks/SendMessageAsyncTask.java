package com.ii.client.ii.AsyncTasks;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.widget.Toast;

import com.ii.client.EchoDetailFragment;
import com.ii.client.ii.Client;
import com.ii.client.ii.Models.OutgoingMessage;

import java.io.IOException;

/**
 * Created by ntkrnlmp.exe on 21.06.2014.
 */
public class SendMessageAsyncTask extends AsyncTask<OutgoingMessage, Void, Void> {

    Client client;
    Exception e;
    Context fragment;

    public SendMessageAsyncTask(Client client, Fragment fragment) {
        this.client = client;
        this.fragment = fragment.getActivity().getApplicationContext();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Toast toast = Toast.makeText(fragment,
                "Сообщение отправляется...",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 50);
        toast.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if(this.e != null) {
            Toast toast = Toast.makeText(fragment,
                    e.getMessage(),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        } else {

            Toast toast = Toast.makeText(fragment,
                    "Сообщение отправлено.",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 50);
            toast.show();
        }
    }

    @Override
    protected Void doInBackground(OutgoingMessage... outgoingMessages) {
        try {
            //client.sendMessage(outgoingMessages[0]);
            if(client.isOfflineMode) {
                client.getDatabase().AddSendLaterMessage(outgoingMessages[0]);
                throw new Exception("Сообщение добавлено в очередь на отправку.");
            } else {
                client.sendMessage(outgoingMessages[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.e = e;
        }
        return null;
    }
}
