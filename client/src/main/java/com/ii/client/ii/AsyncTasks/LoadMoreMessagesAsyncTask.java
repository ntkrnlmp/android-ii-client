package com.ii.client.ii.AsyncTasks;

import android.os.AsyncTask;

import com.ii.client.ii.IDownloadProgressUpdate;
import com.ii.client.ii.Models.Message;

/**
 * Created by ntkrnlmp.exe on 24.08.2014.
 */
public abstract class LoadMoreMessagesAsyncTask extends AsyncTask<Object, String, Iterable<Message>> implements IDownloadProgressUpdate {
    @Override
    public void raiseProgressUpdate(String... values) {
        publishProgress(values);
    }
}
