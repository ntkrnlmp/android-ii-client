package com.ii.client.ii;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.ListView;

import com.ii.client.MessageFactory;
import com.ii.client.ii.AsyncTasks.AsyncDownloadBundleResponce;
import com.ii.client.ii.AsyncTasks.RetrieveBundleTask;
import com.ii.client.ii.AsyncTasks.RetrieveEchoListTask;
import com.ii.client.ii.AsyncTasks.RetrieveMessagesIdTask;
import com.ii.client.ii.AsyncTasks.RetrieveSortedByThemeBundle;
import com.ii.client.ii.Database.IDatabase;
import com.ii.client.ii.Models.Message;
import com.ii.client.ii.Models.OutgoingMessage;

import org.apache.http.NameValuePair;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */

public class Client {

    public static final int RETRIEVE_MODE_DIRECT = 0;
    public static final int RETRIEVE_MODE_REVERSE = 1;

    String node;
    String echoListUrl;
    String auth;
    int maxBundleCount;
    static IDatabase database;
    String currentEchoArea;

    HttpURLConnection webClient;

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getEchoListUrl() {
        return echoListUrl;
    }

    public void setEchoListUrl(String echoListUrl) {
        this.echoListUrl = echoListUrl;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public int getMaxBundleCount() {
        return maxBundleCount;
    }

    public void setMaxBundleCount(int maxBundleCount) {
        this.maxBundleCount = maxBundleCount;
    }

    public IDatabase getDatabase() {
        return database;
    }

    public boolean isOfflineMode;

    public void setDatabase(IDatabase database) {
        this.database = database;
    }

    //FIXME: костыль чтобы при отправке своих сообщений до обновления нотификатор не сходил с ума
    private int fixingNewMessageValue = 0;


    public Client(IDatabase database) {
        this.database = database;
        initFields();
    }

    private void initFields() {
        maxBundleCount = 60;
    }

    public Iterable<String> getEchos() throws IOException {
        URL link = null;

        if(node == null)
            return null;


        link = new URL(node.substring(0, node.indexOf("/u/")) + "/list.txt");

        //data = new RetrieveEchoListTask().execute(link).get();
        URLConnection webClient = (HttpURLConnection) link.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(webClient.getInputStream()));

        String line;
        StringBuilder dataBuilder = new StringBuilder();

        while((line = bufferedReader.readLine()) != null) {
            dataBuilder.append(line + "\n");
        }

        bufferedReader.close();

        Iterable<String> echos = getEchosByList(dataBuilder.toString());

        database.CreateEchos(echos);
        return echos;
    }

    private Iterable<String> getEchosByList(String html) {
        ArrayList<String> echos = new ArrayList<String>();

        if(html.length() <= 0)
            return null;

        // im.1407:273:эха общения, июль 2014

        String[] splittedEchos = html.split("\n");
        for(int i = 0; i < splittedEchos.length; ++i) {
            echos.add(splittedEchos[i].substring(0, splittedEchos[i].indexOf(":")));
        }

        return echos;
    }

    public void setCurrentEchoArea(String echoName) {
        this.currentEchoArea = echoName;
    }

    public String getCurrentEchoArea() {
        return currentEchoArea;
    }

    public Iterable<String> getMessageListFromEcho(String echoName) throws ExecutionException, InterruptedException, IOException {
        URL url = null;
        url = new URL(node + "e/" + echoName);

        List<String> data;

        URLConnection webClient = null;
        webClient = (HttpURLConnection) url.openConnection();

        BufferedReader bufferedReader = null;
        bufferedReader = new BufferedReader(new InputStreamReader(webClient.getInputStream()));

        String line;
        data = new ArrayList<String>();

        while((line = bufferedReader.readLine()) != null) {
            data.add(line);
        }

        bufferedReader.close();

        // только заголовок эхи
        if (data.size() == 1) {
            return null;
        }

        if(data.size() > 1) {
            data.remove(0);
            return data;
        }
        else
            throw new IOException("Невозможно загрузить список сообщений эхоконференции.\nПроверьте правильность URL ноды, наличие интернет соединения, доступность ноды.");
    }

    public int getNewMessagesCount(String echoName) throws InterruptedException, ExecutionException, IOException {
        List<String> messagesId = (ArrayList<String>) getMessageListFromEcho(echoName);
        if (messagesId == null)
            return -1;

        List<String> existingMessagesId = (List<String>) database.GetEchoMessageList(echoName);
        if(existingMessagesId != null) {
            int lastExistingId = messagesId.indexOf(existingMessagesId.get(existingMessagesId.size()-1));
            if(lastExistingId >= 0 && lastExistingId < messagesId.size() - 1) {
                return messagesId.size() - 1 - lastExistingId  - fixingNewMessageValue;
            }
            else {
                return 0;
            }
        }
        return -2;

        //return ((List<String>)getMessageListFromEcho(echoName)).size();
    }

    public int getFixingNewMessageValue() {
        return fixingNewMessageValue;
    }

    public Iterable<Message> getBundleReverse(String echoName, int startDownloadFrom, int count) throws InterruptedException, ExecutionException, IOException {
        List<String> messagesId = (ArrayList<String>) getMessageListFromEcho(echoName);
        if (messagesId == null)
            return null;

        List<String> existingMessagesId = (List<String>) database.GetEchoMessageList(echoName);
        if(startDownloadFrom == 0) {
            if (existingMessagesId != null) {
                int lastExistingId = messagesId.indexOf(existingMessagesId.get(existingMessagesId.size() - 1));
                if (lastExistingId >= 0 && lastExistingId < messagesId.size() - 1) {
                    database.SetEchoMessageList(echoName, messagesId);
                    messagesId = messagesId.subList(lastExistingId + 1, messagesId.size());
                } else {
                    return null;
                }
            } else {
                database.SetEchoMessageList(echoName, messagesId);
            }
        }

        fixingNewMessageValue = 0;

        StringBuilder bundleUrl = new StringBuilder();
        ArrayList<Message> messages = new ArrayList<Message>();
        int downloadedCount = 0;
        String url = node + "m/" + echoName;


        int m_size = messagesId.size();

        String line;

        URLConnection webClient = null;

        int needToDownloadCount; // count
        if(m_size > count) {
            needToDownloadCount = count;
        }
        else {
            needToDownloadCount = m_size;
        }
        int upperValue;

        // оптимизация для цикла
        int startFrom = m_size - 1 - startDownloadFrom;

        int startValue;
        int endValue;
        StringBuilder sb;

        String patternStr = "([a-zA-Z+-/0-9=]+:[a-zA-Z+-/0-9=]+)";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher match;
        BufferedReader bufferedReader = null;


        // фикс ошибки url is too large - запрашиваем по MaxBundleCount сообщений в рамках одного бандла
        while(downloadedCount < needToDownloadCount)
        {
            // либо выкачиваем MaxBundleCount, либо остаток, который меньше чем MaxBundleCount
            upperValue = downloadedCount + maxBundleCount < needToDownloadCount ? maxBundleCount : needToDownloadCount - downloadedCount;
            startValue = startFrom - downloadedCount;
            if(maxBundleCount > m_size) {
                endValue = 0;
            }
            else {
                endValue = startFrom - downloadedCount - upperValue;
                if(endValue < 0) {
                    endValue = 0;
                }
            }
            //for (int i = downloadedCount; i < downloadedCount + upperValue; ++i)
            for(int i = startValue; i >= endValue; --i)
            {
                bundleUrl.append("/" + messagesId.get(i));
            }

            //FIXME вынести отсюда нахер это все
            webClient = (HttpURLConnection) new URL(url + bundleUrl.toString()).openConnection();

            bufferedReader = new BufferedReader(new InputStreamReader(webClient.getInputStream()));

            sb = new StringBuilder();

            while((line = bufferedReader.readLine()) != null) {
                sb.append(line + "\n");
            }

            bufferedReader.close();


            String raw = sb.toString();
            //FIXME
            raw = raw.substring(raw.indexOf(':') + 1, raw.length());
            match = pattern.matcher(raw);

            String plainText = null;

            while(match.find()) {
                String id = match.group().substring(0, match.group().indexOf(':'));
                byte[] bytes = Base64.decode(match.group().substring(match.group().indexOf(':'), match.group().length()), Base64.DEFAULT);
                plainText = new String(bytes, "UTF-8");

                Message m = MessageFactory.ParseMessage(plainText);
                m.setMsgId(id);
                messages.add(m);
            }

            bundleUrl = new StringBuilder();
            downloadedCount += maxBundleCount;

            getDatabase().AddMessagesToEcho(echoName, messages, startDownloadFrom+(downloadedCount-upperValue));
        }


        return messages;
    }

    public Iterable<Message> getBundleDirect(String echoName, int startDownloadFrom, int count, IDownloadProgressUpdate updater) throws InterruptedException, ExecutionException, IOException {
        List<String> messagesId = (ArrayList<String>) getMessageListFromEcho(echoName);
        if (messagesId == null)
            return null;

        List<String> existingMessagesId = (List<String>) database.GetEchoMessageList(echoName);
        if(startDownloadFrom == 0) {
            if (existingMessagesId != null) {
                int lastExistingId = messagesId.indexOf(existingMessagesId.get(existingMessagesId.size() - 1));
                if (lastExistingId >= 0 && lastExistingId < messagesId.size() - 1) {
                    database.SetEchoMessageList(echoName, messagesId);
                    messagesId = messagesId.subList(lastExistingId + 1, messagesId.size());
                } else {
                    return null;
                }
            } else {
                database.SetEchoMessageList(echoName, messagesId);
            }
        }

        fixingNewMessageValue = 0;

        StringBuilder bundleUrl = new StringBuilder();
        ArrayList<Message> messages = new ArrayList<Message>();
        int downloadedCount = 0;
        String url = node + "m/" + echoName;

        int m_size = messagesId.size();

        String line;

        URLConnection webClient = null;

        int needToDownloadCount; // count
        if(m_size > count) {
            needToDownloadCount = count;
        }
        else {
            needToDownloadCount = m_size;
        }
        int upperValue;

        // оптимизация для цикла
        int startFrom = startDownloadFrom;

        int startValue;
        int endValue;
        StringBuilder sb;

        String patternStr = "([a-zA-Z+-/0-9=]+:[a-zA-Z+-/0-9=]+)";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher match;
        BufferedReader bufferedReader = null;

        // фикс ошибки url is too large - запрашиваем по MaxBundleCount сообщений в рамках одного бандла
        while(downloadedCount < needToDownloadCount)
        {
            // либо выкачиваем MaxBundleCount, либо остаток, который меньше чем MaxBundleCount
            upperValue = downloadedCount + maxBundleCount < needToDownloadCount ? maxBundleCount : needToDownloadCount - downloadedCount;
            startValue = startFrom + downloadedCount;
            if(maxBundleCount > m_size) {
                endValue = m_size;
            }
            else {
                endValue = startFrom + downloadedCount + upperValue;
                if(endValue > m_size) {
                    endValue = m_size;
                }
            }
            //for (int i = downloadedCount; i < downloadedCount + upperValue; ++i)
            for(int i = startValue; i < endValue; ++i)
            {
                bundleUrl.append("/" + messagesId.get(i));
            }

            webClient = (HttpURLConnection) new URL(url + bundleUrl.toString()).openConnection();

            bufferedReader = new BufferedReader(new InputStreamReader(webClient.getInputStream()));

            sb = new StringBuilder();

            while((line = bufferedReader.readLine()) != null) {
                if(updater != null) {
                    updater.raiseProgressUpdate("Загружаю сообщения (" + startValue++ + " из "+ needToDownloadCount + ")");
                }
                sb.append(line + "\n");
            }

            bufferedReader.close();


            String raw = sb.toString();
            //FIXME
            raw = raw.substring(raw.indexOf(':') + 1, raw.length());
            match = pattern.matcher(raw);

            String plainText = null;

            while(match.find()) {
                String id = match.group().substring(0, match.group().indexOf(':'));
                byte[] bytes = Base64.decode(match.group().substring(match.group().indexOf(':'), match.group().length()), Base64.DEFAULT);
                plainText = new String(bytes, "UTF-8");

                Message m = MessageFactory.ParseMessage(plainText);
                m.setMsgId(id);
                messages.add(m);
            }

            bundleUrl = new StringBuilder();
            downloadedCount += maxBundleCount;

            getDatabase().AddMessagesToEcho(echoName, messages, startDownloadFrom+downloadedCount);
        }


        return messages;
    }


    public void sendMessage(OutgoingMessage message) throws Exception {
        String url = node + "point";
        String msg = Base64.encodeToString(message.toString().getBytes(), Base64.URL_SAFE);
        msg = msg.substring(0, msg.length() - 1);

        HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
        OutputStream os = connection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        String data = String.format("pauth=%s&tmsg=%s", auth, msg);
        writer.write(data);
        writer.flush();
        writer.close();

        connection.connect();

        InputStream response = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(response, "UTF-8"));

        StringBuilder sb = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null)  {
            sb.append(line + "\n");
        }

        line = sb.toString();
        if(line.contains("error:no auth")) {
            throw new Exception("Неверный auth-токен");
        }

        fixingNewMessageValue++;
    }

}
