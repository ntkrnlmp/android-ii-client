package com.ii.client.ii;

/**
 * Created by ntkrnlmp.exe on 24.08.2014.
 */
public interface IDownloadProgressUpdate {
    void raiseProgressUpdate(String... values);
}
