package com.ii.client.ii;

import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import com.ii.client.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HelpActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        WebView view = (WebView)findViewById(R.id.webView);
        //view.loadDataWithBaseURL("file:///android_res/raw/help.html", null, "text/html", "utf-8", null);

        InputStream ins = getResources().openRawResource(
                getResources().getIdentifier("raw/help",
                        "raw", getPackageName()));

        StringBuffer buf = new StringBuffer();

        BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                buf.append(line);
            }

            String summary = "<html><body>" + buf.toString() + "</body></html>";
            view.loadDataWithBaseURL("x-data://base",summary, "text/html", "utf-8", null);

        } catch (IOException e) {
            e.printStackTrace();

            Toast.makeText(this, "Не удалось прочитать файл справки.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.help, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
