package com.ii.client.ii.Models;

import java.util.Date;
/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */
public class Message {
    /*
        *  1 	разные 	тэги в виде: имя/значение/имя2/значение. в текущем виде используются только для repto и для рекомендуемого идентификатора ii/ok
           2 	echoarea 	основная эхоконференция, в которую помещается сообщение (серверные xc более не поддерживаются)
           3 	date 	число секунд от эпохи unix, в utc
           4 	msgfrom 	отправитель сообщения
           5 	addr 	адрес отправителя сообщения
           6 	msgto 	пользователь, которому предназначено сообщение (либо All)
           7 	subj 	тема сообщения
           8 	- 	пустая строка
           9 и далее 	msg 	текст сообщения
        */
    private String msgId;
    private String tags;
    private String echoArea;
    private Date date;
    private String messageFrom;
    private String address;
    private String messageTo;
    private String subject;
    private String text;
    private boolean local;
    private int localId;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getTags() {
        return tags != null ? tags : "";
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getEchoArea() {
        return echoArea;
    }

    public void setEchoArea(String echoArea) {
        this.echoArea = echoArea;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getIsLocal() {
        return local;
    }

    public void setIsLocal(boolean value) {
        local = value;
    }

    public void setLocalId(int value) {
        localId = value;
    }

    public int getLocalId() {
        return localId;
    }

    public Message() {
        local = false;
    }

    @Override
    public String toString() {
        String newLine = System.getProperty("line.separator");

        StringBuilder sb = new StringBuilder();
        sb.append(getTags()).append(newLine);
        sb.append(getEchoArea()).append(newLine);
        sb.append(date.getTime() / 1000).append(newLine);
        sb.append(getMessageFrom()).append(newLine);
        sb.append(getAddress()).append(newLine);
        sb.append(getSubject()).append(newLine);
        sb.append(getText()).append(newLine);

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 18 + msgId.hashCode()
                        + tags.hashCode()
                        + echoArea.hashCode()
                        + date.hashCode()
                        + messageFrom.hashCode()
                        + address.hashCode()
                        + messageTo.hashCode()
                        + subject.hashCode()
                        + text.hashCode();

        return result;
    }

    @Override
    public boolean equals(Object o) {

        if(o instanceof Message) {
            Message another = (Message)o;

            return this.hashCode() == another.hashCode();
        }

        return super.equals(o);
    }
}
