package com.ii.client.ii.AsyncTasks;

import com.ii.client.ii.Models.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ntkrnlmp.exe on 18.06.2014.
 */
public interface AsyncDownloadBundleResponce {
    void bundleDownloadFinished(List<Message> messages);
}
