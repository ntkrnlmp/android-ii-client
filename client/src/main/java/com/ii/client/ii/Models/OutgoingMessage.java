package com.ii.client.ii.Models;

/**
 * Created by ntkrnlmp.exe on 20.06.2014.
 */
public class OutgoingMessage {
    public String echoArea;
    public String messageTo;
    public String subject;
    public String replyTo;
    public String message;
    public int localId;

    public String getEchoArea() {
        return echoArea;
    }

    public void setEchoArea(String echoArea) {
        this.echoArea = echoArea;
    }

    public String getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("%s\n%s\n%s\n\n%s", echoArea,
                                              messageTo,
                                              subject,
                                              replyTo == null ? message : "@repto:" + replyTo + "\n" + message);
    }
}
