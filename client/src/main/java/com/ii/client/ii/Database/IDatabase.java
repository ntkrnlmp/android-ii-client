package com.ii.client.ii.Database;

import com.ii.client.ii.IDownloadProgressUpdate;
import com.ii.client.ii.Models.Message;
import com.ii.client.ii.Models.OutgoingMessage;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */
public interface IDatabase {
    void CreateEcho(String echoName);
    void CreateEchos(Iterable<String> echos);

    void AddMessageToEcho(String echoName, Message message);
    void AddMessage(Message message);
    void AddMessagesToEcho(String echoName, Iterable<Message> messages, int index);
    void AddSendLaterMessage(OutgoingMessage message);
    void UpdateSendLaterMessage(OutgoingMessage message);

    Iterable<String> GetEchoMessageList(String echoName);
    void SetEchoMessageList(String echoName, Iterable<String> messageList);
    Iterable<Message> GetMessages(Iterable<String> messagesHashId);
    Iterable<Message> GetMessages(String echoName);
    Iterable<Message> GetMessages(String echoName, int offset, int count, IDownloadProgressUpdate updater) throws Exception;
    Message GetMessage(String messageHashId);
    boolean CanOffline();

    Iterable<String> GetEchos();
}
