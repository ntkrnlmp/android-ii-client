package com.ii.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Image;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ii.client.ii.Models.Message;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */
class MessagesAdapter extends ArrayAdapter<Message> {

    private List<Message> items;
    public int count;
    private final DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    private boolean isLoading = false;
    private String userName;

    public MessagesAdapter(Context context,int textViewResourceId, List<Message> objects) {
        super(context, textViewResourceId, objects);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        userName = pref.getString("username_preference", null);

        this.items = objects;
        if(items.size() > 40)
            count = 40;
        else
            count = items.size();
    }

    @Override
    public int getCount() {
        return count;
    }

    public boolean getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean value) {
        isLoading = value;
    }

    public void getMore() {
        count += 20;
        if(count >= items.size())
            count = items.size();
    }

    public List<Message> getItems() {
        return items;
    }

    public boolean canMore() {
        return items.size() - count != 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater vi =  LayoutInflater.from(getContext());
            convertView = vi.inflate(R.layout.detail_row, null);
            holder = new ViewHolder();
            holder.subject = (TextView) convertView.findViewById(R.id.Subject);
            holder.from = (TextView) convertView.findViewById(R.id.From);
            holder.to = (TextView) convertView.findViewById(R.id.To);
            holder.arrow = (ImageView) convertView.findViewById(R.id.imageView2);
            holder.text = (TextView) convertView.findViewById(R.id.Text);
            holder.date = (TextView) convertView.findViewById(R.id.Date);
            holder.clock = (ImageView) convertView.findViewById(R.id.imageView3);
            holder.backgroud = (RelativeLayout) convertView.findViewById(R.id.rel_layout);
            holder.wait = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Message m = items.get(position);
        holder.subject.setText(m.getSubject());
        holder.from.setText(m.getMessageFrom());
        holder.to.setText(m.getMessageTo());
        holder.date.setText(df.format(m.getDate()));

        if(!m.getTags().contains("repto"))
        {
            holder.backgroud.setBackgroundColor(Color.parseColor("#1700FF90"));
        }
        else
        {
            if(userName != null ) {
                if(m.getMessageTo().contains(userName)) {
                    holder.backgroud.setBackgroundColor(Color.parseColor("#171550FF"));
                } else {
                    holder.backgroud.setBackgroundColor(Color.TRANSPARENT);
                }
            } else {
                holder.backgroud.setBackgroundColor(Color.TRANSPARENT);
            }
        }

        if(m.getIsLocal()) {
            holder.backgroud.setBackgroundColor(Color.parseColor("#60FFF317"));
            holder.wait.setVisibility(View.VISIBLE);
        } else {
            holder.wait.setVisibility(View.GONE);
        }

        String str = m.getText();
        if(str.length() > 150) {
            holder.text.setText(str.substring(0, 150) + "... \n(Читать далее)\n");
        }
        else {
            holder.text.setText(str);
        }
        return convertView;
    }

    static class ViewHolder {
        TextView subject;
        TextView from;
        TextView text;
        TextView date;
        TextView to;
        ImageView arrow;
        ImageView clock;
        ImageView wait;
        RelativeLayout backgroud;
    }


}
