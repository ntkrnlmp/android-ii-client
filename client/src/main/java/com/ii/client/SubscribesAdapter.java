package com.ii.client;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ntkrnlmp.exe on 22.06.2014.
 */
public class SubscribesAdapter extends ArrayAdapter<String> {

    ArrayList<String> items;

    public SubscribesAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        items = (ArrayList<String>) objects;
    }

    public ArrayList<String> getItems() {
        return items;
    }

    public void addItem(String str) {
        items.add(str);
        notifyDataSetChanged();
    }

    public void deleteItem(int index) {
        items.remove(index);
        notifyDataSetChanged();
    }

    public void replaceItem(int position, String text) {
        items.set(position, text);
        notifyDataSetChanged();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater vi =  LayoutInflater.from(getContext());
            convertView = vi.inflate(R.layout.subscribe_row, null);

            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.subscribe_text_view);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //Fill the views in your row
        holder.text.setText(items.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView text;
    }


}
