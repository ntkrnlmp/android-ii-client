package com.ii.client;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ntkrnlmp.exe on 18.06.2014.
 */
public class MessageFragment extends Fragment {

    private static final int DEFINITION = 1;
    private static final String quote_tag = "<font color=\"#3355CC\"><i>";
    private static final String quote_close_tag = "</i></font>";
    private static final String a_href_tag = "<a href=\"";
    private static final String a_href_close_tag = "\">";
    private static final String end_a = "</a>";

    String from;
    String to;
    String subj;
    String id;
    String date;
    String quote;
    int localId;

    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.message_fragment, container, false);
        String text = getArguments().getString("Text");
        from = getArguments().getString("From");
        to = getArguments().getString("To");
        subj = getArguments().getString("Subject");
        id = getArguments().getString("Id");
        date = getArguments().getString("Date");
        localId = getArguments().getInt("LocalId", 0);

        TextView subjectView = (TextView) rootView.findViewById(R.id.textView);
        TextView messageFromTextView = (TextView) rootView.findViewById(R.id.MessageFromTextView);
        TextView messageToTextView = (TextView) rootView.findViewById(R.id.MessageToTextView);
        textView = (TextView) rootView.findViewById(R.id.textView3);
        TextView dateView = (TextView) rootView.findViewById(R.id.textView4);

        subjectView.setText(subj);
        messageFromTextView.setText(from);
        messageToTextView.setText(to);

        String richText = makeRichText(text);
        textView.setText(Html.fromHtml(richText));
        dateView.setText(date);

        quote = null;

        textView.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Called when action mode is first created. The menu supplied
                // will be used to generate action buttons for the action mode

                // Here is an example MenuItem
                menu.add(0, DEFINITION, 0, "Цитировать").setIcon(android.R.drawable.ic_menu_set_as).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // Called when an action mode is about to be exited and
                // destroyed
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case DEFINITION:
                        try {
                            int min = 0;
                            int max = textView.getText().length();
                            if (textView.isFocused()) {
                                final int selStart = textView.getSelectionStart();
                                final int selEnd = textView.getSelectionEnd();

                                min = Math.max(0, Math.min(selStart, selEnd));
                                max = Math.max(0, Math.max(selStart, selEnd));
                            }
                            // Perform your definition lookup with the selected text
                            quote = textView.getText().subSequence(min, max).toString();
                            actionAnswer();
                            // Finish and close the ActionMode
                            mode.finish();
                        }
                        catch (Exception e)
                        {
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                            dialog.setTitle("Ошибка");

                            dialog.setMessage(e.getMessage());

                            dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });

                            dialog.create().show();
                        }
                        return true;
                    default:
                        break;
                }
                return false;
            }

        });


        setHasOptionsMenu(true);
        return rootView;
    }

    private String makeRichText(String source) {
        // parse html links\

        try {
            StringBuilder result = new StringBuilder(source);

            int index = 0;

            //выделение цитирования
            index = 0;
            while ((index = result.indexOf(">", index)) >= 0) {
                result.insert(index, quote_tag);
                index += quote_tag.length() + 1;

                while (result.charAt(index) != '\n' && result.charAt(index) != '\r') {
                    index++;

                    if (index >= result.length()) {
                        index--;
                        break;
                    }
                }

                result.insert(index, quote_close_tag);
                index += quote_close_tag.length() + 1;
            }

            replaceAll(result, "\n", "<br>");
            replaceAll(result, "\r", "<br>");

            return result.toString();
        }
        catch (OutOfMemoryError oom) {
            Toast.makeText(getActivity(), "Недостаточно памяти для форматирования сообщения.\nСообщение выведено в неформатированном виде.", Toast.LENGTH_SHORT).show();
        }

        return source;
    }

    static void replaceAll(StringBuilder builder, String from, String to)
    {
        int index = builder.indexOf(from);
        while (index != -1)
        {
            builder.replace(index, index + from.length(), to);
            index += to.length(); // Move to the end of the replacement
            index = builder.indexOf(from, index);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.view_message_action, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reply:
                actionAnswer();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void actionAnswer() {

        Bundle bundle = new Bundle();

        bundle.putString("To", from);
        bundle.putString("Subject", subj);
        bundle.putString("Id", id);
        bundle.putString("Quote", quote);
        bundle.putInt("LocalId", localId);
        bundle.putString("Message", textView.getText().toString());

        NewMessageFragment fragment = new NewMessageFragment();
        fragment.setArguments(bundle);

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.echo_detail_container, fragment)
                .addToBackStack(null)
                .commit();
    }
}
