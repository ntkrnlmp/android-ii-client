package com.ii.client;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ii.client.dummy.DummyContent;
import com.ii.client.ii.Client;
import com.ii.client.ii.ClientSingleton;
import com.ii.client.ii.Database.PlainTextDatabase;
import com.ii.client.ii.Database.SQLiteDataBase;


/**
 * An activity representing a list of Messages. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link EchoDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link EchoListFragment} and the item details
 * (if present) is a {@link EchoDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link EchoListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class EchoListActivity extends FragmentActivity
        implements EchoListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("Debug", "EchoListActivity: onCreate");

        setContentView(R.layout.activity_echo_list);

        if (findViewById(R.id.echo_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((EchoListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.echo_list))
                    .setActivateOnItemClick(true);
        }

        // TODO: If exposing deep links into your app, handle intents here.
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Debug", "EchoListActivity: onResume");
    }

    /**
     * Callback method from {@link EchoListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(String id) {
        ClientSingleton.getClient().setCurrentEchoArea(DummyContent.ITEM_MAP.get(id).content);
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(EchoDetailFragment.ARG_ITEM_ID, id);
            EchoDetailFragment fragment = new EchoDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.echo_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, EchoDetailActivity.class);
            detailIntent.putExtra(EchoDetailFragment.ARG_ITEM_ID, id);
            startActivity(detailIntent);
        }
    }
}
