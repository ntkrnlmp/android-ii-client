package com.ii.client;

import com.ii.client.ii.Models.Message;

import java.util.Date;

/**
 * Created by ntkrnlmp.exe on 12.06.2014.
 */
public class MessageFactory {

    public static Message ParseMessage(String rawMessage)
    {
        Message message = new Message();
        // немного кривая реализация
        String[] splittedMessage = rawMessage.split("\n");
        message.setTags(splittedMessage[0]);
        message.setEchoArea(splittedMessage[1]);
        message.setDate(new Date(Long.parseLong(splittedMessage[2]) * 1000));//FIXME make local time
        message.setMessageFrom(splittedMessage[3]);
        message.setAddress(splittedMessage[4]);
        message.setMessageTo(splittedMessage[5]);
        message.setSubject(splittedMessage[6]);
                //message.Tags.Contains("repto") && !splittedMessage[6].Contains("Re:") ? "Re: " + splittedMessage[6] : splittedMessage[6];

        // далее могут быть переносы строки, а split теоретически мог поделить и их
        // поэтому надо собрать обратно строки, являющиеся текстом сообщения
        StringBuilder tmp = new StringBuilder();
        for (int i = 8; i < splittedMessage.length; ++i)
        {
            tmp.append(splittedMessage[i] + "\n");
        }

        message.setText(tmp.toString());

//        if (msgId != null)
//            message.MsgId = msgId;

        return message;
    }
}
