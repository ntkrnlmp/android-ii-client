package com.ii.client;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ntkrnlmp.exe on 22.06.2014.
 */
public class SubscribesPrefFragment extends Fragment {
    ListView listView;

    final int EditMenuItem = 1;
    final int DeleteMenuItem = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.subscribes_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.subscribes_fragment, container, false);

        List<String> subscribes = new ArrayList<String>();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String tmp = pref.getString("subscribes", null);
        if(tmp != null) {
            String[] splitted = tmp.split(";");
            for(int i=0; i < splitted.length; ++i) {
                subscribes.add(splitted[i]);
            }
            splitted = null;
        }

        TextView emptyView = (TextView)rootView.findViewById(R.id.empty);
        listView = (ListView)rootView.findViewById(R.id.subscribes_listview);
        listView.setEmptyView(emptyView);

        listView.setAdapter(new SubscribesAdapter(getActivity(), android.R.layout.simple_list_item_1, subscribes));

        registerForContextMenu(listView);

        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_subscribe_add:
                showInputDialog(null, -1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void showInputDialog(String text, int position) {
        final int pos = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("Новая подписка");

        final EditText input = new EditText(this.getActivity());

        if(text != null) {
            input.setText(text);
        }

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(pos == -1) {
                    ((SubscribesAdapter) listView.getAdapter()).addItem(input.getText().toString().trim());
                } else {
                    ((SubscribesAdapter) listView.getAdapter()).replaceItem(pos, input.getText().toString().trim());
                }
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, EditMenuItem, 0, "Редактировать");
        menu.add(0, DeleteMenuItem, 0, "Удалить");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case DeleteMenuItem:
                ((SubscribesAdapter) listView.getAdapter()).deleteItem(info.position);
                return true;
            case EditMenuItem:
                showInputDialog(listView.getItemAtPosition(info.position).toString(), info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        StringBuilder subscr = new StringBuilder();
        ArrayList<String> items = ((SubscribesAdapter)listView.getAdapter()).getItems();
        for(String str : items) {
            subscr.append(str + ";");
        }


        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("subscribes", subscr.toString());
        editor.commit();

    }
}
