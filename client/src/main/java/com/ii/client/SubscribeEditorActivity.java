package com.ii.client;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by ntkrnlmp.exe on 22.06.2014.
 */
public class SubscribeEditorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscribe_editor_activity);

        // Show the Up button in the action bar.
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.

            SubscribesPrefFragment fragment = new SubscribesPrefFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.subscribe_container, fragment)
                    .commit();
        }

    }
}
