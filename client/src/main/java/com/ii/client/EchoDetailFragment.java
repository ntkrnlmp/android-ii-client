package com.ii.client;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;


import com.ii.client.dummy.DummyContent;
import com.ii.client.ii.AsyncTasks.AsyncDownloadBundleResponce;
import com.ii.client.ii.AsyncTasks.LoadMoreMessagesAsyncTask;
import com.ii.client.ii.AsyncTasks.RetrieveBundleTask;
import com.ii.client.ii.Client;
import com.ii.client.ii.ClientSingleton;
import com.ii.client.ii.Database.PlainTextDatabase;
import com.ii.client.ii.IDownloadProgressUpdate;
import com.ii.client.ii.Models.Message;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * A fragment representing a single Echo detail screen.
 * This fragment is either contained in a {@link EchoListActivity}
 * in two-pane mode (on tablets) or a {@link EchoDetailActivity}
 * on handsets.
 */
public class EchoDetailFragment extends Fragment implements AsyncDownloadBundleResponce {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    Client client;

    public final Object Locker = new Object();
    /**
     * The dummy content this fragment is presenting.
     */
    private DummyContent.DummyItem mItem;
    MessagesAdapter adapter;
    ListView listView;

    RetrieveBundleTask retrieveBundleTask;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EchoDetailFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }

        client = ClientSingleton.getClient();
        retrieveBundleTask = new RetrieveBundleTask(client, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_echo_detail, container, false);

        listView = (ListView) rootView.findViewById(R.id.listview1);

        if(retrieveBundleTask == null) {
            retrieveBundleTask = new RetrieveBundleTask(ClientSingleton.getClient(), this);
        }

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            if(adapter == null) {
                refreshMessages();
            }
            else {
                setUp(listView);
            }
        }

        return rootView;
    }

    @Override
    public void bundleDownloadFinished(List<Message> messages) {
        try {
            if (messages != null) {
                adapter = new MessagesAdapter(getActivity(), android.R.layout.simple_list_item_1, messages);
                setUp(listView);
            } else {
                ArrayList<Message> dbMessages = (ArrayList<Message>) ClientSingleton.getClient().getDatabase().GetMessages(ClientSingleton.getClient().getCurrentEchoArea());
                if (dbMessages != null) {
                    adapter = new MessagesAdapter(getActivity(), android.R.layout.simple_list_item_1, dbMessages);
                    setUp(listView);
                }
            }
        }
        catch (Exception e) {
            // путь наименьшего сопротивления, ага
            // ошибка синхронизации, messages == null.
            e.printStackTrace();
        }
    }

    public static boolean isNetworkAvailable(Context context)
    {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() == null;
    }

    void refreshMessages() {
        // java - говно. я не умею её готовить
        client.isOfflineMode = isNetworkAvailable(getActivity());

        if (retrieveBundleTask.getStatus() == AsyncTask.Status.PENDING || retrieveBundleTask.getStatus() == AsyncTask.Status.FINISHED) {
            retrieveBundleTask = new RetrieveBundleTask(client, this);
            retrieveBundleTask.execute(mItem.content, 0, client.getMaxBundleCount());
        }
    }


    void setUp(ListView listView) {
        listView.setAdapter(adapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view,
                                 int firstVisible, int visibleCount, final int totalCount) {
                boolean loadMore = firstVisible + visibleCount >= totalCount;

                if(!((MessagesAdapter)view.getAdapter()).getIsLoading()) {
                    if (loadMore) { // если отображаются не все элементы адаптера
                        final MessagesAdapter adapter = ((MessagesAdapter) view.getAdapter());
                        if (!adapter.canMore()) {   // если вывели все элементы адаптера => требуется подгрузка
                            if (!adapter.getIsLoading()) {  // превент дабл лоадинг
                                // проверяем надо ли вообще что-то загружать
                                List<String> db_messages = ((List<String>) client.getDatabase().GetEchoMessageList(client.getCurrentEchoArea()));
                                if (db_messages.size() > adapter.count) {
                                    adapter.setIsLoading(true);

                                    new LoadMoreMessagesAsyncTask()  {
                                        Exception e;
                                        ProgressDialog dialog;

                                        @Override
                                        protected void onPreExecute() {
                                            dialog = new ProgressDialog(getActivity());
                                            dialog.setTitle("Загружаю сообщения");
                                            dialog.setMessage("Пожалуйста, подождите...");
                                            dialog.setCancelable(false);
                                            dialog.show();
                                        }

                                        @Override
                                        protected void onProgressUpdate(String... values) {
                                            dialog.setMessage(values[0]);
                                        }

                                        @Override
                                        protected void onPostExecute(Iterable<Message> messages) {
                                            MessagesAdapter adp = adapter;
                                            dialog.dismiss();
                                            if (this.e != null) {
                                                if (this.e != null) {
                                                    adp.setIsLoading(false);
                                                    showError(e);
                                                    return;
                                                }
                                            }

                                            List<Message> currentItem = adp.getItems();

                                            if(currentItem == null) {
                                                showError(new Exception("Message adapter is null."));
                                                adp.setIsLoading(false);
                                                return;
                                            }

                                            if(messages == null) {
                                                // сообщений нет
                                                adp.setIsLoading(false);
                                                return;
                                            }

                                            try {

                                                currentItem.addAll(startFrom, (java.util.Collection<? extends Message>) messages);
                                                /*
                                                int first_message = ((List<Message>) messages).indexOf(currentItem.get(0));
                                                if (first_message > 0 && first_message != -1) {
                                                    currentItem.addAll(0, ((List<Message>) messages).subList(0, first_message));
                                                }

                                                int last_message = ((List<Message>) messages).indexOf(currentItem.get(currentItem.size() - 1));
                                                if (last_message < ((List<Message>) messages).size() - 1 && last_message != -1) {
                                                    currentItem.addAll(currentItem.size(), ((List<Message>) messages).subList(last_message, ((List<Message>) messages).size()));
                                                }*/

                                            }
                                            catch (Exception e)
                                            {
                                                showError(new Exception("Ошибка при обработке загруженных сообщений.\n" + e.getMessage()));
                                                adp.setIsLoading(false);
                                                return;
                                            }

                                            adp.notifyDataSetChanged();
                                            adp.setIsLoading(false);
                                        }

                                        private void showError(Exception e) {
                                            final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                                            dialog.setTitle("Ошибка");

                                            dialog.setMessage(e.getMessage());

                                            dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    MessagesAdapter adp = adapter;
                                                    adp.setIsLoading(false);
                                                    dialogInterface.dismiss();
                                                }
                                            });

                                            dialog.create().show();
                                        }

                                        int startFrom;
                                        int count;

                                        @Override
                                        protected Iterable<Message> doInBackground(Object... objects) {
                                            Iterable<Message> messages = null;
                                            try {
                                                startFrom = (Integer) objects[1];
                                                count = (Integer) objects[2];
                                                client.isOfflineMode = isNetworkAvailable(getActivity());
                                                messages = client.getDatabase().GetMessages((String) objects[0], (Integer) objects[1], (Integer) objects[2], this);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                this.e = e;
                                                messages = null;
                                            }

                                            return messages;
                                        }
                                    }.execute(mItem.content, adapter.count, client.getMaxBundleCount());
                                    return;
                                } else {
                                    adapter.setIsLoading(false);
                                    return;
                                }
                            } else {
                                return;
                            }
                        }

                        adapter.getMore();
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Bundle bundle = new Bundle();

                Message m = (Message)adapterView.getAdapter().getItem(position);

                bundle.putString("From", m.getMessageFrom());
                bundle.putString("Text", m.getText());
                bundle.putString("To", m.getMessageTo());
                bundle.putString("Subject", m.getSubject());
                bundle.putString("Id", m.getMsgId());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                bundle.putString("Date", sdf.format(m.getDate()));
                bundle.putInt("LocalId", m.getLocalId());

                MessageFragment fragment = new MessageFragment();
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.echo_detail_container, fragment)
                        .addToBackStack("echodetail")
                        .commit();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_activity_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_message:
                NewMessageFragment fragment = new NewMessageFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.echo_detail_container, fragment)
                        .addToBackStack("echodetail")
                        .commit();
                return true;
            case R.id.action_refresh:
                refreshMessages();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
